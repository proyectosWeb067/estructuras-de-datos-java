package ListaSimplementeenlazada;

import javax.swing.JOptionPane;

/**
 *
 * @author Yio
 */
public class Listas_Java {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Lista l = new Lista();

        int opcion = 0, valorIngresado = 0;
        do {
            try {

                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "1. Agregar al inicio."
                        + "\n2. Agregar al final.\n3. Borrar inicio.\n4. Borrar final.\n5. Borrar elemento especifico."
                        + "\n6. Buscar un elemento.\n7. Imprimir lista.\n8. Salir", "Menu Opciones", 3));
                switch (opcion) {
                    case 1:
                        try {
                            valorIngresado = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar Valor:", "Insertando Inicio", 3));
                            l.insertarNodoInicio(valorIngresado);
                        } catch (NumberFormatException e) {
                            e.getMessage();
                        }
                        break;
                    case 2:
                        try {
                            valorIngresado = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar Valor:", "Insertando Final", 3));
                            l.insertarNodoFinal(valorIngresado);
                        } catch (NumberFormatException e) {
                            e.getMessage();
                        }
                        break;
                    case 3:
                           l.borrarInicio(); 
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        try {
                            valorIngresado = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar Valor:", "Buscar valor", 3));
                            l.buscarNodo(valorIngresado);
                        } catch (NumberFormatException e) {
                            e.getMessage();
                        }

                        break;
                    case 7:
                        l.recorreLista();
                        break;
                    case 8:
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion incorrecta");
                }
            } catch (Exception e) {
                e.getMessage();
            }
        } while (opcion != 8);

    }

}
