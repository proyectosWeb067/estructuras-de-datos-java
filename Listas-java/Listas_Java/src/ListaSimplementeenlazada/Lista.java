/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaSimplementeenlazada;

/**
 *
 * @author Yio
 */
public class Lista {

    private Nodo inicio, fin;

    public Lista() {
        this.inicio = null;
        this.fin = null;
    }

    //varificar si lista esta vacia
    public boolean listaVacia() {
        if (this.inicio == null && this.fin == null) {
            return true;
        }
        return false;
    }

    //insertando al inicio
    public void insertarNodoInicio(int valor) {
        this.inicio = new Nodo(valor, this.inicio); //la primera vez inicio antes =(NULL)
        if (this.fin == null) {
            this.fin = this.inicio;
        }
    }

    //agregar al final
    public void insertarNodoFinal(int valor) {
        Nodo nuevoFinal = new Nodo(valor);
        if (!listaVacia()) {                            //lista no vacia 
            this.fin.setSiguiente(nuevoFinal);
            this.fin = nuevoFinal;
        } else{                                         //lista vacia
            this.fin = nuevoFinal;
            this.inicio = this.fin; 
        }
    }

    //buscar un nodo
    public void buscarNodo(int valor) {
        int vecesEnLista = 0;
        if (!listaVacia()) {
            while (this.inicio != null) {
                if (this.inicio.getValor() == valor) {
                    vecesEnLista += 1;
                }
                this.inicio = this.inicio.getSiguiente();
            }
            System.out.println("El valor " + valor + " esta en lista " + vecesEnLista + " veces.");
        }
    }

    //borrar inicio
    public void borrarInicio() {
        if (!listaVacia()) {
            if (this.inicio == this.fin) {
                this.inicio = null;
            } else {
                Nodo nuevoInicio = this.inicio.getSiguiente();
                this.inicio = null;
                this.inicio = nuevoInicio;
            }
        }
    }

    //recorriendo lista
    public void recorreLista() {
        Nodo auxiliar = this.inicio;
        while ( auxiliar!= null) {

            System.out.print("[" + auxiliar.getValor() + "]-->");
            auxiliar = auxiliar.getSiguiente();
        }
        System.out.println("null");
    }

    //encapsulamiento
    public Nodo getInicio() {
        return inicio;
    }

    public void setInicio(Nodo inicio) {
        this.inicio = inicio;
    }

    public Nodo getFin() {
        return fin;
    }

    public void setFin(Nodo fin) {
        this.fin = fin;
    }

}
