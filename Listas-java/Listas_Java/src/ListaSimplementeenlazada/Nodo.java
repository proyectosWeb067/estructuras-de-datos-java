/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaSimplementeenlazada;

/**
 *
 * @author Yio
 */
public class Nodo {

    private int valor;
    private Nodo siguiente;

    public Nodo(int valor) {
        this.valor = valor; 
        this.siguiente = null;
    }

    public Nodo(int valor,Nodo puntero){
        this.valor = valor;
        this.siguiente = puntero;
    }
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    
}
