/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasdoblementeenlazadas;

/**
 *
 * @author Yio
 */
public class ListaDoble {

    private Nodo inicio, fin;

    public ListaDoble() {
        this.inicio = null;
        this.fin = null;
    }

    //lista vacia
    public boolean listaVacia() {

        return inicio == null;
    }

    //insertar al incio
    public void insertarInicio(int valor) {
        if (!listaVacia()) {
            Nodo anteriorNodoInicio = this.inicio;
            this.inicio = new Nodo(valor);
            this.inicio.setSiguiente(anteriorNodoInicio);
            anteriorNodoInicio.setAnterior(this.inicio);
        } else {
            this.inicio = new Nodo(valor);
            if (this.fin == null) {
                this.fin = this.inicio;
            }
        }
    }

    //insertar al final
    public void insertarFinal(int valor) {
        if (!listaVacia()) {
            Nodo anteriorNodoFin = this.fin;
            this.fin = new Nodo(valor);
            anteriorNodoFin.setSiguiente(this.fin);
            this.fin.setAnterior(anteriorNodoFin);
        } else {
            this.fin = new Nodo(valor);
            if (this.inicio == null) {
                this.inicio = this.fin;
            }
        }
    }

    //recorrer Lista
    public void recorrerDesdeInicio() {
        Nodo auxiliar = this.inicio;
        while (auxiliar != null) {
            System.out.print(" <--[" + auxiliar.getValor() + "]--> ");
            auxiliar = auxiliar.getSiguiente();
        }
    }

    //recorrer lista desde el final
    public void recorrerDesdeFinal() {
        Nodo auxiliar = this.fin;
        while (auxiliar != null) {
            System.out.print(" <--[" + auxiliar.getValor() + "]--> ");
            auxiliar = auxiliar.getAnterior();
        }
    }

    //buscar un elemento comenzando desde  inicio
    public void buscarElemento(int valor) {
        Nodo auxiliar = this.inicio;
        int vecesElemento = 0;
        while (auxiliar != null) {
            if (auxiliar.getValor() == valor) {
                vecesElemento+=1;
            }
            auxiliar = auxiliar.getSiguiente();
        } 
        System.out.println("[" +valor + "]" + " está en lista "+vecesElemento+" veces");
    }
    
    //eliminar inicio
    public void eliminarInicio(){
        
    }
}
